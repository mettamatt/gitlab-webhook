Gitlab Push Webhook
==========

## Assumptions

- For Push events only. When a push is detected, this script will attempt to run a `git pull` command. 
- Works with multiple repos on the same server, assuming your using the same branch setup for each project. 
- The script is written and PHP and works on Ubuntu 16.04.

## Gitlab.com Setup
### Creating a Webhook
  - Login to your Gitlab account and go the project you want to automate git pulls for. 
  - From the main project page, navigate to `Settings > Integrations`
  ![GitLab Webhook Integrations Screen](https://www.evernote.com/l/AAGLNKUPkaNH6YhPMbUwsiiFI8-QkcgrqXMB/image.png)
  - The page will look something like the above image. You need to enter:
    - **The URL where this code will live** so GitLab can access it. An SSL connection is preferred. Use [Let's Encrypt](https://letsencrypt.org/getting-started/) on your server for free SSL certs. Install the SSL cert with CertBot.
    - **A Secret Token.** Generate a nice long token you don't share with anyone. You'll add this token into the `config.php` to verify GitLab is making the request. I wish we could verify GitLab's IP address, but they rotate about 20 different IPs so this is our best bet.
    - Make sure the checkbox is checked for the `Push events` trigger. 
    - If you choose an SSL connection, make sure the `Enable SSL verification` is checked. 
    - Click the `Add Webhook` submit button
  - Once your webhooks is created, use the `Test` button to send fake Push events to the URL of your webhook, and watch/tail the `webhook.log` to verify everything is working. Note: you'll need to manually create the `webhook.log` file by hand first.

## Installation

Clone this repository to the URL where you defined the webhook to live in the Gitlab.com Setup steps above. 

Make a copy of the `sample.config.php` file. 

```metta@myserver:~$ cp sample.config.php config.php```

Edit your config.php

Set `$ACCESS_TOKEN` to the Secret Token you defined in the webhook. 

Most people will keep the deploy branch setting the same. 
  
```$DEPLOY_BRANCH = 'refs/heads/master';```

For `LOG_FILE`, it's best to define this file to live outside a publicly accessible directory. Be sure to **create the file first**, and set the permissions. For my personal setup, webhook.log lives in /var/log/ and has www-data for it's owner.

```-rw-rw----  1 www-data        www-data           3585 Jun 17 15:55 webhook.log```

To define the repositories, the array is `Project ID > local working directory`. You can find the Project ID on your project settings page in GitLab. We use `Project ID` rather than `Project Name` because the name may change but the  ID is permanent. 

Your `local working directory` is where you cloned the project on your server, and it's where the script will `cd` before running `git pull`. Here's an example

```// Gitlab Project ID => local working directory (no trailing slash)
$DEFINED_REPOS = array(
  '3399856' => '/srv/src/myproject1',
  '5802247' => '/srv/src/myproject2',
);```

### Testing

On your server, use the `tail` command to watch the `webhook.log` file you created. 

```metta@myserver:~$ tail -f /var/log/webhook.log```

Then go back to Gitlab and on your navigate to `Settings > Integrations` for your project click the Test button on the webhook you created and watch the `webhook.log` for the event. 

## Permission Denied Errors?

If you run into permission denied errors when the script attempts to execute the git command, you may need to add the www user to your sudoers list. You can often find the name of the user by looking at who owns your www/public_html folder for you web server root. I tend to use `www-data` as the user.

Edit /etc/sudoers to add

```www-data ALL = NOPASSWD: /usr/bin/git```
