<?php
require_once('config.php');

/* Grab user token and ip address from headers */
$client_token = $_SERVER["HTTP_X_GITLAB_TOKEN"];
$client_ip    = $_SERVER['REMOTE_ADDR'];

_log("===========================".PHP_EOL);
_log('Auto-Deploy Start on ['.date("Y-m-d H:i:s").'] from ['.$client_ip.']'.PHP_EOL);

/* Test secret token */
if ($client_token !== $ACCESS_TOKEN) {
    echo "error 403";
    _log("Invalid token: [{$client_token}]".PHP_EOL);
    exit(0);
}

/* Get json data */
$json = file_get_contents('php://input');
$data = json_decode($json, true);

/* Grab the branch and project id */
$branch = $data["ref"];
$pid    = $data["project_id"];

if ($branch !== $DEPLOY_BRANCH) {
  _log("Branches do not match: [{$branch}] and $DEPLOY_BRANCH".PHP_EOL);
  exit(0);
}

if (array_key_exists($pid, $DEFINED_REPOS)) {
  $deploy_to = $DEFINED_REPOS[$pid];
  if (is_dir($deploy_to) && (file_exists($deploy_to . "/.git"))) {
    $cmd = "cd $deploy_to && sudo /usr/bin/git pull origin master 2>&1";
    _log("Running: $cmd".PHP_EOL);
    _log(shell_exec($cmd));
  }
  else {
    _log("Directory does not exist or .git file not found: [{$deploy_to}]".PHP_EOL);
    exit(0);
  }
}
else {
  _log("Project id [{$pid}] is not a configured repo.".PHP_EOL);
}

_log("Auto-Deploy Complete".PHP_EOL);

/**
 * Log a message to a file. Expected constant named LOG_FILE to be 
 * defined in config.php
 */
function _log($message) {
  file_put_contents(
    LOG_FILE, 
    $message,
    FILE_APPEND
  );
}

?>

